# # # Input
# # # \n stands for line break
# # username = input("Please enter your name:\n")
# # print(f"Hi {username}! Welcome to Python Short Course!")

# # #typecasting
# # num1 = int(input("Please enter the first number: "))
# # num2 = int(input("Please enter the second number: "))
# # print(f"The sum of num1 and num2 is {num1 + num2}")

# # # Section If-Else Statement
# # test_num = 75
# # # in Python, we use colon : 
# # if test_num >= 60 :
# #     print("Test Passed")
# # else : 
# #     print("Test Failed")
    
# # # if-else chains
# # test_num2 = int(input("Please enter a number for testing:\n"))
# # if test_num2 > 0:
# #     print("The number is positive")
# # elif test_num2 == 0:
# #     print("The number is zero")
# # else:
# #     print("The number is negative")
    
# # # Mini Activity
    
# # test_div_num = int(input("Please enter a number: \n"))
# # if test_div_num % 3 == 0 and test_div_num % 5 == 0:
# #     print("The number is divisible by 3 and 5")
# # elif test_div_num % 3 == 0:
# #     print("The number is divisible by 3")
# # elif test_div_num % 5 == 0:
# #     print("The number is divisible by 5")
# # else: 
# #     print(f"{test_div_num} is not divisible by both 3 and 5")
    
# # # [Section] Loops




# # While Loop
# # performs a code block as long as the condition is true
# i = 1
# while i <= 5:
#     print(f"Current Value: {i}")
#     i += 1
    
# # For loop is ( For in )
# fruits = [ "apple", "banana", "orange"]
# for indiv_fruit in fruits:
#     print(indiv_fruit)


# for x in range(6) :
#     print(f"Current Value: {x}")
    
# for x in range(1, 10) :
#     print(f"Current Value: {x}")
    
# for x in range(1, 10, 2) :
#     print(f"Current Value: {x}")
    
# Break and Continue Statement

j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1
    
# Continue Statement

k = 1
while k < 6:
    if k == 3:
        continue
    print(k)
    k += 1
    
    
    